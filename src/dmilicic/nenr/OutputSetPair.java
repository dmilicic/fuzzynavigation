package dmilicic.nenr;

public class OutputSetPair {

	private Output output;
	private FuzzySet set;
	
	public OutputSetPair(Output output, FuzzySet set) {
		this.output = output;
		this.set = set;
	}

	public Output getOutput() {
		return output;
	}

	public FuzzySet getSet() {
		return set;
	}
	
}
