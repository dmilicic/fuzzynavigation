package dmilicic.nenr;

public class Decoder {
	
	public static enum DecoderMode {
		CENTER_OF_AREA;
	}

	public static double decode(double[] domain, double[] measures, DecoderMode mode) {
		double result = 0.0;
		if(mode == DecoderMode.CENTER_OF_AREA)
			result = decodeCenterOfArea(domain, measures);
		return result;
	}
	
	private static double decodeCenterOfArea(double[] domain, double[] measures) {
		double denominator = 0.0;
		double numerator = 0.0;
		for(int i = 0; i < domain.length; i++) {
			denominator += measures[i];
			numerator += domain[i] * measures[i];
		}
		
		double result = 0.0;
		
		if(denominator > 10e-9)
			result = numerator / denominator;
		
		return result;
	}
}
