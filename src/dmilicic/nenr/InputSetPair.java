package dmilicic.nenr;

public class InputSetPair {
	
	private Input input;
	private FuzzySet set;
	
	public InputSetPair(Input input, FuzzySet set) {
		this.input = input;
		this.set = set;
	}

	public Input getInput() {
		return input;
	}

	public FuzzySet getSet() {
		return set;
	}
}
