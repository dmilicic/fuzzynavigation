package dmilicic.nenr;

public class DomainGenerator {
	
	public static double[] generate(double start, double end, double step) {
		double domainSize = end - start;
		int len = (int) (domainSize / step);
		
		double[] domain = new double[len];
		for (int i = 0; i < domain.length; i++) {
			domain[i] = start;
			start += step;
		}
		
		return domain;
	}

}
