package dmilicic.nenr;

public interface InferenceEngine {
	
	public double implication(Rule rule, double y);
	public double s_norm(InputSetPair[] sets);
	public double t_norm(InputSetPair[] sets);
	public double conclusion(Rule[] rules, double y[]);
}
