package dmilicic.nenr;

public class Rule {
	
	private InputSetPair[] antecedent;
	private FuzzySet inference;
	
	public Rule(InputSetPair[] antecedent, FuzzySet inference) {
		this.antecedent = antecedent;
		this.inference = inference;
	}

	public InputSetPair[] getAntecedent() {
		return antecedent;
	}

	public FuzzySet getInference() {
		return inference;
	}
}
