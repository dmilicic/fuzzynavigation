package dmilicic.nenr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import dmilicic.nenr.FuzzySet.FuzzyFunctions;

public class Main {
	
	public static void main(String[] args) throws IOException {

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

	    int L=0, D=0, LK=0, DK=0, V=0, S=0, acc, rudder;
	    String line = null;
		while(true){
			if((line = input.readLine())!=null){
				if(line.charAt(0)=='K') break;
				Scanner s = new Scanner(line);
				L = s.nextInt();
				D = s.nextInt();
				LK = s.nextInt();
				DK = s.nextInt();
				V = s.nextInt();
				S = s.nextInt();
	        }
			
			
//			L = 100;
//			D = 100;
//			LK = 100;
//			DK = 100;
//			V = 20;
//			S = 1;


	        // fuzzy magic ...
			
			// Attach input values 
			Input leftDist = new Input("L", L);
			Input leftAngledDist = new Input("LK", LK);
			Input rightDist = new Input("D", D);
			Input rightAngledDist = new Input("DK", DK);
			Input speed = new Input("V", V);
			Input direction = new Input("S", S);
			
			// Distance to coast fuzzy sets
			FuzzySet close = new FuzzySet("Close distance", 20.0, 50.0, null, FuzzyFunctions.L);
			FuzzySet mediumDist = new FuzzySet("Medium distance", 30.0, 50.0, 70.0, FuzzyFunctions.LAMBDA);
			FuzzySet far = new FuzzySet("Far distance", 20.0, 50.0, null, FuzzyFunctions.GAMMA);
			
			// Speed fuzzy set
			FuzzySet slow = new FuzzySet("Slow speed", 0.0, 20.0, null, FuzzyFunctions.L);
			FuzzySet medium = new FuzzySet("Medium speed", 10.0, 30.0, 50.0, FuzzyFunctions.LAMBDA);
			FuzzySet fast = new FuzzySet("Fast speed", 30.0, 50.0, null, FuzzyFunctions.GAMMA);
			
			// Direction fuzzy set
//			FuzzySet rightDirection = new FuzzySet("Right direction", 20.0, 50.0, null, FuzzyFunctions.L);
//			FuzzySet wrongDirection = new FuzzySet("Wrong direction", 30.0, 50.0, 70.0, FuzzyFunctions.LAMBDA);
	
			
			// Accelerator fuzzy set
			FuzzySet NB = new FuzzySet("Negative big acceleration", -15.0, -5.0, null, FuzzyFunctions.L);
			FuzzySet NS = new FuzzySet("Negative small acceleration", -10.0, -5.0, 0.0, FuzzyFunctions.LAMBDA);
			FuzzySet Z = new FuzzySet("Zero acceleration", -5.0, 0.0, 5.0, FuzzyFunctions.LAMBDA);
			FuzzySet PS = new FuzzySet("Positive small acceleration", 0.0, 5.0, 10.0, FuzzyFunctions.LAMBDA);
			FuzzySet PB = new FuzzySet("Positive big acceleration", 5.0, 15.0, null, FuzzyFunctions.GAMMA);
			
			
			// Rudder fuzzy set
			FuzzySet rightSharp = new FuzzySet("Left sharp rudder", -60.0, -30.0, null, FuzzyFunctions.L);
			FuzzySet right = new FuzzySet("Left rudder", -60.0, -30.0, 0.0, FuzzyFunctions.LAMBDA);
			FuzzySet forward = new FuzzySet("Forward rudder", -30.0, 0.0, 30.0, FuzzyFunctions.LAMBDA);
			FuzzySet left = new FuzzySet("Right rudder", 0.0, 30.0, 60.0, FuzzyFunctions.LAMBDA);
			FuzzySet leftSharp = new FuzzySet("Right sharp rudder", 30.0, 60.0, null, FuzzyFunctions.GAMMA);
			
			/****** Rules *******/
			
			// arraylist that can easily add rules as they grow  
			ArrayList<Rule> rudderRules = new ArrayList<>();
			ArrayList<Rule> accRules = new ArrayList<>();
			
			// IF LK is CLOSE THEN rudder is RIGHT SHARP
			InputSetPair[] pairs1 = new InputSetPair[1];
			pairs1[0] = new InputSetPair(leftAngledDist, close);
			FuzzySet out1 = rightSharp;
			rudderRules.add(new Rule(pairs1, out1));
			
			// IF DK is CLOSE THEN rudder is LEFT SHARP
			InputSetPair[] pairs2 = new InputSetPair[1];
			pairs2[0] = new InputSetPair(rightAngledDist, close);
			FuzzySet out2 = leftSharp;			
			rudderRules.add(new Rule(pairs2, out2));
			
			// IF DK is FAR AND LK is FAR AND V is SLOW THEN acceleration is PB
			InputSetPair[] pairs3 = new InputSetPair[3];
			pairs3[0] = new InputSetPair(rightAngledDist, far);
			pairs3[1] = new InputSetPair(leftAngledDist, far);
			pairs3[2] = new InputSetPair(speed, slow);
			FuzzySet out3 = PS;
			accRules.add(new Rule(pairs3, out3));
			
			
			// IF DK is FAR AND LK is FAR AND D is CLOSE THEN acceleration is PB
			InputSetPair[] pairs4 = new InputSetPair[3];
			pairs4[0] = new InputSetPair(rightAngledDist, far);
			pairs4[1] = new InputSetPair(leftAngledDist, far);
			pairs4[2] = new InputSetPair(rightDist, close);
			FuzzySet out4 = PB;
			accRules.add(new Rule(pairs4, out4));
			
			
			// IF DK is FAR AND LK is FAR AND L is CLOSE THEN acceleration is PB
			InputSetPair[] pairs5 = new InputSetPair[3];
			pairs5[0] = new InputSetPair(rightAngledDist, far);
			pairs5[1] = new InputSetPair(leftAngledDist, far);
			pairs5[2] = new InputSetPair(leftDist, close);
			FuzzySet out5 = PB;
			accRules.add(new Rule(pairs5, out5));
			
			
			// IF DK is MEDIUM AND V is FAST THEN acceleration is NB
			InputSetPair[] pairs6 = new InputSetPair[2];
			pairs6[0] = new InputSetPair(rightAngledDist, mediumDist);
			pairs6[1] = new InputSetPair(speed, fast);
			FuzzySet out6 = NB;
			accRules.add(new Rule(pairs6, out6));
			
			
			// IF LK is MEDIUM AND V is FAST THEN acceleration is NB
			InputSetPair[] pairs7 = new InputSetPair[2];
			pairs7[0] = new InputSetPair(leftAngledDist, mediumDist);
			pairs7[1] = new InputSetPair(speed, fast);
			FuzzySet out7 = NB;
			accRules.add(new Rule(pairs7, out7));
			
			// ############### END RULES ######################
			
			// Convert the arraylist to a regular list of rules
			Rule[] allRudderRules = new Rule[rudderRules.size()];
			for(int i = 0; i < allRudderRules.length; i++) {
				allRudderRules[i] = rudderRules.get(i);
			}
			
			Rule[] allAccRules = new Rule[accRules.size()];
			for(int i = 0; i < allAccRules.length; i++) {
				allAccRules[i] = accRules.get(i);
			}
			
			/***** Acceleration domain *******/
			
			double[] accDomain = DomainGenerator.generate(-15.0, 15.0, 0.1);
			
			/***** Rudder domain *******/
			
			double[] rudderDomain = DomainGenerator.generate(-90.0, 90.0, 0.1);
			
			
			/******* INFERENCE ENGINE **********/
			
			// Construct our inference engine
//			MinimumInferenceEngine engine = new MinimumInferenceEngine();
			ProductInferenceEngine engine = new ProductInferenceEngine();
			
			
			// Draw a conclusion based on rules and inputs
			rudder = (int) engine.conclusion(allRudderRules, rudderDomain);
			acc = (int) engine.conclusion(allAccRules, accDomain);
			

//			System.out.println();
	        System.out.println(acc + " " + rudder);
	        System.out.flush();
	   }
	}

}
