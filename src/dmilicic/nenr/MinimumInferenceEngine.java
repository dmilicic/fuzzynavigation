package dmilicic.nenr;

import dmilicic.nenr.Decoder.DecoderMode;

public class MinimumInferenceEngine implements InferenceEngine {

	@Override
	public double implication(Rule rule, double y) {
		double result = t_norm(rule.getAntecedent());
		double y_value = rule.getInference().calcValue(y);
		result = Math.min(result, y_value);
		return result;
	}

	@Override
	public double s_norm(InputSetPair[] sets) {
		double result = 0;
		for(int i = 0; i < sets.length; i++) {
			double value = sets[i].getInput().getValue();
			result = Math.max(result, sets[i].getSet().calcValue(value));
		}
		return result;
	}

	@Override
	public double t_norm(InputSetPair[] sets) {
		double result = Double.MAX_VALUE;
		for(int i = 0; i < sets.length; i++) {
			double value = sets[i].getInput().getValue();
			result = Math.min(result, sets[i].getSet().calcValue(value));
		}
		return result;
	}

	@Override
	public double conclusion(Rule[] rules, double y[]) {
		double result[] = new double[y.length];
		for(int i = 0; i < y.length; i++) {
			for(int j = 0; j < rules.length; j++) {
				result[i] = Math.max(result[i], implication(rules[j], y[i]));
			}
		}
		
		return Decoder.decode(y, result, DecoderMode.CENTER_OF_AREA);
	}

}
