package dmilicic.nenr;

public class Input {
	
	private String name;
	private double value;
	
	public Input(String name, double value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public double getValue() {
		return value;
	}
}
