package dmilicic.nenr;

public class FuzzySet {
	
	public static enum FuzzyFunctions {
		LAMBDA, GAMMA, L
	}
	
	private Double alpha, beta, gamma;
	private FuzzyFunctions function;
	private String title;
	
	public FuzzySet(String title, Double a, Double b, Double c, FuzzyFunctions function) {
		this(a,b,c,function);
		this.title = title;
	}

	public FuzzySet (Double a, Double b, Double c, FuzzyFunctions function) {
		
		this.function = function;
		
		if(function == FuzzyFunctions.LAMBDA) {
			if(a == null || b == null || c == null)
				throw new IllegalArgumentException();
			alpha = a;
			beta = b;
			gamma = c;
		}
		
		if(function == FuzzyFunctions.L || function == FuzzyFunctions.GAMMA) {
			if(a == null || b == null)
				throw new IllegalArgumentException();
			alpha = a;
			beta = b;
			gamma = null;
		}
	}
	
	public String getTitle() {
		return title;
	}
	
	public double calcValue(double x) {
		double result = 0;
		if(function == FuzzyFunctions.GAMMA)
			result = calcGamma(x);
		else if(function == FuzzyFunctions.L)
			result = calcL(x);
		else if(function == FuzzyFunctions.LAMBDA)
			result = calcLambda(x);
		return result;
	}
	
	private double calcLambda(double x) {
		double result = 0;
		if(x < alpha)
			result = 0.0;
		else if(x >= alpha && x < beta)
			result = (x - alpha) * 1.0 / (beta - alpha);
		else if(x >= beta && x < gamma)
			result = (gamma - x) * 1.0 / (gamma - beta);
		else if(x >= gamma)
			result = 0;
			
		return result;
	}
	
	private double calcGamma(double x) {
		double result = 0;
		if(x < alpha)
			result = 0;
		else if(x >= alpha && x < beta)
			result = (x - alpha) * 1.0 / (beta - alpha);
		else if(x >= beta)
			result = 1.0;
		
		return result;
	}
	
	private double calcL(double x) {
		double result = 0;
		if(x < alpha)
			result = 1.0;
		else if(x >= alpha && x < beta)
			result = (beta - x) * 1.0 / (beta - alpha);
		else if(x >= beta)
			result = 0;
		
		return result;
	}
	
}
