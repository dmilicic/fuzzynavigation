The goal of this project was to build a navigation system for the simple ship simulator. The navigation system is built using Fuzzy Logic techniques. You can choose between three different maps for the ship to navigate through. This is done by changing the second line to 1, 2 and 3 respectively in the Simulator/config.txt

To start the program simply start this jar file:  Simulator/Simulator.jar